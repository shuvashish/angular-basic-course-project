import { CourseProjectTheBasicsPage } from './app.po';

describe('course-project-the-basics App', () => {
  let page: CourseProjectTheBasicsPage;

  beforeEach(() => {
    page = new CourseProjectTheBasicsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
